#!/usr/bin/env bash

if [ ! -f pagerduty.key ]
then
	echo "'pagerduty.key' file not found"
	exit 1
elif [ ! -f input.csv ]
then
	echo "'input.csv' file not found"
	exit 1
fi

if ! command -v jq &> /dev/null; then
	echo "'jq' cannot be found"
	exit 1
elif ! command -v curl &> /dev/null; then
	echo "'curl' cannot be found"
	exit 1
fi

# https://support.pagerduty.com/docs/generating-api-keys#generating-a-personal-rest-api-key
API_USER_KEY="$(cat pagerduty.key)"
APAC_CMOC1_SCHEDULE="PGUP5OB"
START_TIME="00:00:00+00:00"
END_END="04:00:00+00:00"

# https://github.com/dylanaraps/pure-bash-bible#percent-encode-a-string
urlencode() {
	local LC_ALL=C
	for (( i = 0; i < ${#1}; i++ )); do
		: "${1:i:1}"
		case "$_" in
			[a-zA-Z0-9.~_-])
				printf '%s' "$_"
				;;

			*)
				printf '%%%02X' "'$_"
				;;
		esac
	done
	printf '\n'
}

declare -A USERS
while read -r LINE
do
	# Find users mentioned in input.csv, and find their Pagerduty IDs
	NAME="$(echo $LINE | awk -F',' '{print $2}' | sed 's/[[:space:]]*$//')"
	URL_ENCODED_NAME=$(urlencode "$NAME")
	USERS[$NAME]=$(curl -s --request GET \
		--url "https://api.pagerduty.com/users?limit=1&query=$URL_ENCODED_NAME"   \
		--header 'Accept: application/vnd.pagerduty+json;version=2' \
		--header "Authorization: Token token=$API_USER_KEY" \
		--header 'Content-Type: application/json' | jq -r .users[0].id )

	# Find the "first" override that is currently in place on the mentioned day. (Assume there is only 1 for now)
	DAY="$(echo $LINE | awk -F',' '{print $1}' | sed 's/[[:space:]]*$//')"
	DAY_OVERRIDE_ID=$(curl -s --request GET \
		--url "https://api.pagerduty.com/schedules/$APAC_CMOC1_SCHEDULE/overrides?since=${DAY}T${START_TIME}&until=${DAY}T${END_TIME}" \
		--header 'Accept: application/vnd.pagerduty+json;version=2' \
		--header "Authorization: Token token=$API_USER_KEY" \
		--header 'Content-Type: application/json' | jq -r .overrides[0].id )

	# Delete the override that is currently in place on the mentioned day
	if [ "$DAY_OVERRIDE_ID" != 'null' ]; then
		curl -s --request DELETE \
			--url "https://api.pagerduty.com/schedules/$APAC_CMOC1_SCHEDULE/overrides/$DAY_OVERRIDE_ID" \
			--header 'Accept: application/json' \
			--header "Authorization: Token token=$API_USER_KEY" \
			--header 'Content-Type: application/json'
	fi
done < input.csv

# Create initial JSON objects
# Schedule uses UTC (+00:00)
while IFS=, read -r DAY USER
do
	NAME=$(echo "$USER" | sed 's/[[:space:]]*$//')
	data1+='{"start":"'${DAY}'T'${START_TIME}'","end":"'${DAY}'T'${END_TIME}'","user": { "id": "'${USERS[$NAME]}'", "type": "user_reference" },"time_zone":"UTC"},'
done < input.csv

# Remove the last trailing comma
data2=$(echo "$data1" | sed 's/\(.*\),/\1 /')

# Create full JSON object for the pagerduty API
data3='{ "overrides": [ '$data2' ] }'

curl -s --request POST \
	--url https://api.pagerduty.com/schedules/$APAC_CMOC1_SCHEDULE/overrides \
	--header 'Accept: application/vnd.pagerduty+json;version=2' \
	--header "Authorization: Token token=$API_USER_KEY" \
	--header 'Content-Type: application/json' \
	--data "$data3" | jq

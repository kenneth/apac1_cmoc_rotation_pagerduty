# How to use

- Install `bash`, `jq`,  `curl`.
- Create a [pagerduty key](https://support.pagerduty.com/docs/generating-api-keys#generating-a-personal-rest-api-key) and put it in a file called `pagerduty.key`
- Create a file called `input.csv` (Might be easier to do this in a spreadsheet)
  - The first column should have the day for the override to occur.
  - The second column is the name of the Support Engineer (See [notes](#Notes) below)
  - See `input.csv.example` for an example of how the file should look in plaintext.
- Run the script. For example, with `bash apac1_cmoc_rotation_pagerduty`

# Notes

The script will look up the name of the Support Engineer using the Pagerduty API. The search should return a unique result, otherwise it may find an
unintended user.

For example, let's assume there are there are two Support Engineers in the team with the names:

- `John Citizen`
- `John Doe`

And you are looking for `John Doe`. Using the name `John` to find `John Doe` will return `John Citizen` instead. Use something more unique like `John
D` or `John Doe` if you want to find the other user.
